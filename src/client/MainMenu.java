package client;

import model.Booking;
import model.Customer;
import model.MovieSession;
import model.User;
import system.BookingSystem;
import system.interfaces.Menu;

import java.awt.*;
import java.util.ArrayList;
import java.util.Scanner;

class MainMenu implements Menu {

    private boolean exited = false;
    private User user;
    private final static BookingSystem bookingSystem = new BookingSystem();

    MainMenu(User user) {
        this.user = user;
    }

    @Override
    public boolean isExited() {
        return this.exited;
    }

    @Override
    public void printMenu() {
        System.out.println("\n**********");
        System.out.println("jMoSS");
        System.out.printf("You are logged in as %s\n", this.user.getUsername());
        System.out.println("**********");

        System.out.println("\n********************************************************");
        System.out.println("Options:");
        System.out.println("1. Show this weeks Movie Sessions at all Cineplex theatres");
        System.out.println("2. Show today’s Movie Sessions at all Cineplex theatres");
        System.out.println("3. Show today’s Movie Sessions for a chosen Cineplex theatre");
        System.out.println("4. Make a Booking for Movie Session");
        System.out.println("5. Delete a Booking for Movie Session");
        System.out.println("6. Find Movie Sessions for Movie");
        System.out.println("7. Find Tickets purchased by customer");
        System.out.println("X. Exit");
        System.out.println("********************************************************\n");
    }

    @Override
    public void handleOption(char option) {
        switch (option) {
            case '1':
                System.out.print(bookingSystem.findThisWeeksMovieSessions());
                break;
            case '2':
                System.out.print(bookingSystem.findTodaysMovieSessions());
                break;
            case '3':
                this.handleShowTodayMovieSessionsForChosenCineplex();
                break;
            case '4':
                this.handleMakeBooking();
                break;
            case '5':
                this.handleDeleteBooking();
                break;
            case '6':
                this.handleFindMovieSessionsForMovie();
                break;
            case '7':
                this.handleFindTicketsPurchasedByCustomer();
                break;
            case 'X':
                this.exited = true;
                Toolkit.getDefaultToolkit().beep();
                System.out.println("\nExiting jMoSS - Goodbye.\n");
                break;
            default:
                System.out.printf("\n\"%c\" is not a valid menu option.\nPlease try again.\n\n", option);
        }
    }

    private void handleShowTodayMovieSessionsForChosenCineplex() {
        final Scanner scanner = new Scanner(System.in);

        System.out.printf("\n%s\n", bookingSystem.findCineplexesInfo());
        System.out.print("Please enter Cineplex ID: ");
        final int id = scanner.nextInt();

        System.out.println(bookingSystem.findTodaysMovieSessionsForCineplex(id));
    }

    private void handleMakeBooking() {
        final Scanner scanner = new Scanner(System.in);

        // get movie session
        System.out.printf("\n%s\n", bookingSystem.findCineplexesInfo());
        System.out.print("Please enter Cineplex ID for which you wish to book at: ");
        final int cineplexId = scanner.nextInt();

        System.out.printf("%s\n", bookingSystem.findMovieSessionsForCineplex(cineplexId));
        System.out.print("Please enter Movie Session ID for which you wish to book: ");
        final int movieSessionId = scanner.nextInt();
        scanner.nextLine();

        final MovieSession movieSession = bookingSystem.findMovieSession(cineplexId, movieSessionId);
        if (movieSession == null) {
            // this is here instead of a loop because there may be no movie sessions
            System.out.println("Incorrect Movie Session ID. Terminating to Main Menu.");
            return;
        }
        if (movieSession.getSeatsAvailable() == 0) {
            System.out.println("Movie Session has no available seats. Terminating to Main Menu.");
            return;
        }

        // multiple bookings
        System.out.print("How many seats to book?: ");
        final int numberOfSeats = scanner.nextInt();
        scanner.nextLine();

        if (movieSession.getSeatsAvailable() < numberOfSeats) {
            System.out.printf("Movie Session only has %s available. Terminating to Main Menu.", movieSession.getSeatsAvailable());
            return;
        }

        // get customer information
        final Customer customer = this.inquireCustomerDetails();

        // booking summary
        System.out.println("\n- Booking Summary -");
        System.out.printf("Cineplex:\n%s\n", bookingSystem.findCineplexInfo(cineplexId));
        System.out.printf("Movie Session:\n%s\n", movieSession.getInfo());
        System.out.printf("Number of Seats to Book:\n%s\n", numberOfSeats);
        System.out.printf("Customer Information:\n%s\n\n", customer.getInfo());

        // booking confirmation
        if (!this.inquireConfirmation("Confirm Booking? (y/n): ", "\nBooking Cancelled.")) {
            // cancel
            return;
        }

        // make booking
        for (int i = 0; i < numberOfSeats; i++) {
            final boolean booked = bookingSystem.makeBooking(cineplexId, movieSessionId, customer);
            if (!booked) {
                // shouldn't be possible but catching nonetheless
                System.out.println("Booking Failed.");
                return;
            }
        }
        System.out.println("\nBooked Successfully.");
    }

    private void handleDeleteBooking() {
        final Scanner scanner = new Scanner(System.in);

        // get customer information
        final Customer customer = this.inquireCustomerDetails();

        // find bookings
        ArrayList<Booking> bookings = bookingSystem.getBookings(customer);
        if (bookings.size() == 0) {
            System.out.println("\nThere are no bookings for the customer. Terminating to Main Menu.");
            return;
        }

        // print bookings
        int bookingIndex = 1;
        System.out.println();
        for (Booking booking : bookings) {
            System.out.printf("- Booking %d -\n%s\n\n", bookingIndex, booking.getInfo());
            bookingIndex++;
        }

        // only one booking found
        if (bookings.size() == 1) {
            if (!this.inquireConfirmation("\nConfirm Booking Deletion? (y/n): ", "\nDelete Booking Cancelled.")) {
                // cancel
                return;
            }

            // delete booking
            final boolean deleteBookingSuccess = bookingSystem.deleteBooking(bookings.get(0).getId());
            if (deleteBookingSuccess) {
                System.out.println("\nSuccessfully Deleted Booking.");
                return;
            } else {
                System.out.println("\nFailed Deleting Booking.");
                return;
            }
        }

        // multiple bookings found (find multiple bookings first)
        if (bookings.size() > 1) {
            System.out.println("\n********************************************************");
            System.out.println("Options:");
            System.out.println("1. Select One Booking for Deletion.");
            System.out.println("2. Delete All Bookings");
            System.out.println("X. Exit");
            System.out.println("********************************************************\n");

            char option;
            while (true) {
                System.out.print("Please select a valid option: ");
                option = Character.toUpperCase(scanner.nextLine().charAt(0));

                switch (option) {
                    case '1':
                        System.out.print("Please enter Booking Number you wish to delete: ");
                        final int bookingNumber = scanner.nextInt();
                        scanner.nextLine();

                        if (bookingNumber > bookings.size()) {
                            System.out.println("\nInvalid Booking Number. Terminating to Main Menu.");
                            return;
                        }

                        if (!this.inquireConfirmation("\nConfirm Booking Deletion? (y/n): ",
                                "\nDelete Booking Cancelled.")) {
                            // cancel
                            return;
                        }

                        final boolean success = bookingSystem.deleteBooking(bookings.get(bookingNumber - 1).getId());
                        if (success) {
                            System.out.println("\nSuccessfully Deleted Booking.");
                            return;
                        } else {
                            System.out.println("\nFailed Deleting Booking.");
                            return;
                        }

                    case '2':
                        if (!this.inquireConfirmation("\nConfirm Booking Deletion? (y/n): ",
                                "\nDelete Booking Cancelled.")) {
                            // cancel
                            return;
                        }
                        // delete bookings
                        for (Booking booking : bookings) {
                            final boolean deleteBookingSuccess = bookingSystem.deleteBooking(booking.getId());
                            if (!deleteBookingSuccess) {
                                System.out.println("\nFailed Deleting Bookings.");
                                return;
                            }
                        }
                        System.out.println("\nSuccessfully Deleted Bookings.");
                        return;
                }
            }
        }
    }

    private void handleFindMovieSessionsForMovie() {
        final Scanner scanner = new Scanner(System.in);

        // get movie title
        System.out.print("\nPlease enter Movie Title: ");
        final String movieTitle = scanner.nextLine();

        System.out.printf("\n%s\n", bookingSystem.findMovieSessionsForMovie(movieTitle));
    }

    private void handleFindTicketsPurchasedByCustomer() {
        final Scanner scanner = new Scanner(System.in);

        // get customer email
        System.out.print("\nPlease enter Customer Email: ");
        final String email = scanner.nextLine();

        System.out.printf("\n%s\n", bookingSystem.findTicketsPurchasedByCustomer(email));
    }

    private Customer inquireCustomerDetails() {
        final Scanner scanner = new Scanner(System.in);

        // get customer information
        System.out.print("\nPlease enter Customer Email: ");
        final String email = scanner.nextLine();

        System.out.print("Please enter Customer Suburb: ");
        final String suburb = scanner.nextLine();

        return new Customer(email, suburb);
    }

    private boolean inquireConfirmation(String questionText, String cancelText) {
        final Scanner scanner = new Scanner(System.in);

        char confirmation = '\0';
        while (confirmation != 'Y') {
            System.out.print(questionText);
            confirmation = Character.toUpperCase(scanner.nextLine().charAt(0));

            if (confirmation == 'N') {
                System.out.println(cancelText);
                return false;
            }
        }
        return true;
    }

}
