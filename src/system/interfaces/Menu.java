package system.interfaces;

public interface Menu {

    void printMenu();

    void handleOption(char option);

    boolean isExited();

}
