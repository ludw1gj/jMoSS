package model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

public class User {

    private String username;
    private String password;
    private boolean isLoggedIn = false;

    public User() {
        // get user values from text file
        ArrayList<String> textLines = new ArrayList<>();

        Path path = Paths.get("user.txt");
        try (Stream<String> lines = Files.lines(path)) {
            lines.forEach(textLines::add);
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
        this.username = textLines.get(0);
        this.password = textLines.get(1);
    }

    public String getUsername() {
        return this.username;
    }

    public boolean isLoggedIn() {
        return this.isLoggedIn;
    }

    public boolean logIn(String username, String password) {
        if (this.username.equals(username) && this.password.equals(password)) {
            this.isLoggedIn = true;
        }
        return this.isLoggedIn;
    }

}
