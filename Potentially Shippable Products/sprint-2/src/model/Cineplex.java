package model;

import system.DateHelper;

import java.util.ArrayList;

public class Cineplex {

    private static int idCount = 0;

    private int id;
    private String location;
    private ArrayList<MovieSession> movieSessions = new ArrayList<>();

    public Cineplex(String location) {
        this.location = location;

        this.id = ++idCount;
    }

    public int getId() {
        return this.id;
    }

    public void addMovieSession(MovieSession movieSession) {
        this.movieSessions.add(movieSession);
    }

    public ArrayList<MovieSession> getMovieSessions() {
        return this.movieSessions;
    }

    public String getThisWeeksMovieSessions() {
        StringBuilder buf = new StringBuilder();

        buf.append(String.format("\n- %s -\n", this.location));
        buf.append("Move Sessions:\n");

        int count = 0;
        for (MovieSession movieSession : this.movieSessions) {
            if (DateHelper.dateIsThisWeek(movieSession.getDate())) {
                buf.append(String.format("%s\n", movieSession.getInfo()));
                count++;
            }
        }

        if (count == 0) {
            buf.append("* No movies running this week found *\n");
        }
        return buf.toString();
    }

    public String getTodaysMovieSessions() {
        StringBuilder buf = new StringBuilder();

        buf.append(String.format("\n- %s -\n", this.location));
        buf.append("Move Sessions:\n");

        int count = 0;
        for (MovieSession movieSession : this.movieSessions) {
            if (DateHelper.dateIsToday(movieSession.getDate())) {
                buf.append(String.format("%s\n", movieSession.getInfo()));
                count++;
            }
        }

        if (count == 0) {
            buf.append("* No movies running today found *\n");
        }
        return buf.toString();
    }

    public String getMovieSessionsList() {
        StringBuilder buf = new StringBuilder();

        buf.append(String.format("\n- %s -\n", this.location));
        buf.append("Move Sessions:\n");

        int count = 0;
        for (MovieSession movieSession : this.movieSessions) {
            buf.append(String.format("%s\n", movieSession.getInfo()));
            count++;
        }

        if (count == 0) {
            buf.append("* No movies running found *\n");
        }
        return buf.toString();
    }

    public String getInfo() {
        return String.format("ID: %d, Location: %s", this.id, this.location);
    }

}
