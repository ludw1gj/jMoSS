package system;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateHelper {

    public static boolean dateIsToday(LocalDateTime date) {
        final LocalDateTime currentTime = LocalDateTime.now();
        return currentTime.getDayOfYear() == date.getDayOfYear() && currentTime.getYear() == date.getYear();
    }

    public static boolean dateIsThisWeek(LocalDateTime date) {
        final LocalDateTime currentTime = LocalDateTime.now();
        return date.getDayOfYear() >= currentTime.getDayOfYear() && date.getDayOfYear() <= currentTime.getDayOfYear() + 6;
    }

    public static String getFormattedDate(LocalDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");
        return date.format(formatter);
    }

    public static LocalDateTime createDate(String date) {
        //  sample date: "10:30 08/09/2018";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");
        return LocalDateTime.parse(date, formatter);
    }

}
