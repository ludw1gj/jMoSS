package client;

import model.User;
import system.interfaces.Menu;

import java.util.Scanner;

class LoginMenu implements Menu {

    private final User user;
    private boolean exited = false;

    LoginMenu(User user) {
        this.user = user;
    }

    @Override
    public void printMenu() {
        System.out.println("**********");
        System.out.println("Welcome to jMoSS");
        System.out.println("Java-based Movie Search System");
        System.out.println("**********");

        System.out.println("\n********************************************************");
        System.out.println("Options:");
        System.out.println("1. Login");
        System.out.println("X. Exit");
        System.out.println("********************************************************\n");
    }

    @Override
    public void handleOption(char option) {
        switch (option) {
            case '1':
                boolean success = this.handleUserLogOn();
                if (!success) {
                    System.out.println("\nInvalid username or password.\nPlease try again.\n");
                }
                this.exited = success;
                break;
            case 'X':
                this.exited = true;
                System.out.println("\nExiting jMoSS - Goodbye.\n");
                break;
            default:
                System.out.printf("\n\"%c\" is not a valid menu option.\nPlease try again.\n\n", option);
        }
    }

    @Override
    public boolean isExited() {
        return this.exited;
    }

    private boolean handleUserLogOn() {
        final Scanner scanner = new Scanner(System.in);

        System.out.print("Please enter username: ");
        String username = scanner.nextLine();

        System.out.print("Please enter password: ");
        String password = scanner.nextLine();

        return this.user.logIn(username, password);
    }
}
