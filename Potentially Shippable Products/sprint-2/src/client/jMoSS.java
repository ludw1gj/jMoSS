package client;

import model.User;
import system.interfaces.Menu;

import java.util.Scanner;

public class jMoSS {

    public static void main(String[] args) {
        final User user = new User();

        processInput(new LoginMenu(user));
        if (user.isLoggedIn()) {
            processInput(new MainMenu(user));
        }
    }

    private static void processInput(Menu menu) {
        final Scanner scanner = new Scanner(System.in);
        char option;
        String input;

        while (!menu.isExited()) {
            menu.printMenu();

            System.out.print("Please select a valid option: ");
            input = scanner.nextLine();

            if (input.length() != 1) {
                System.out.printf("\n\"%s\" is not a valid menu option.\nPlease try again.\n\n", input);
                continue;
            }
            option = Character.toUpperCase(input.charAt(0));
            menu.handleOption(option);
        }
    }

}
