# jMoSS
A Java-based Movie Search System

# Notes
## Default User
Username: john<br>
Password: secret 

Values available from user.txt

# Sprints

## Sprint 1
* Setting up required user facing Classes:<br>
jMoSS, LoginMenu, MainMenu

* Setting up required BookingSystem Class and the Menu Interface

* Setting up required model Classes:<br>
Cineplex, Movie, MovieSession, User

* Login Menu
* Main Menu
    * (1) Show this weeks Movie Sessions at all Cineplex theatres
    * (2) Show today’s Movie Sessions at all Cineplex theatres
* Login Functionality
