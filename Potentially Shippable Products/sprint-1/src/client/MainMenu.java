package client;

import model.User;
import system.BookingSystem;
import system.interfaces.Menu;

class MainMenu implements Menu {

    private boolean exited = false;
    private User user;
    private static BookingSystem bookingSystem = new BookingSystem();

    MainMenu(User user) {
        this.user = user;
    }

    @Override
    public boolean isExited() {
        return this.exited;
    }

    @Override
    public void printMenu() {
        System.out.println("\n**********");
        System.out.println("jMoSS");
        System.out.printf("You are logged in as %s\n", this.user.getUsername());
        System.out.println("**********");

        System.out.println("\n********************************************************");
        System.out.println("Options:");
        System.out.println("1. Show this weeks Movie Sessions at all Cineplex theatres");
        System.out.println("2. Show today’s Movie Sessions at all Cineplex theatres");
        System.out.println("X. Exit");
        System.out.println("********************************************************\n");
    }

    @Override
    public void handleOption(char option) {
        switch (option) {
            case '1':
                System.out.print(bookingSystem.getThisWeeksMovieSessions());
                break;
            case '2':
                System.out.print(bookingSystem.getTodaysMovieSessions());
                break;
            case 'X':
                this.exited = true;
                System.out.println("\nExiting jMoSS - Goodbye.\n");
                break;
            default:
                System.out.printf("\n\"%c\" is not a valid menu option.\nPlease try again.\n\n", option);
        }
    }
}
