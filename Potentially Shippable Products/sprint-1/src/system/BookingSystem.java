package system;

import model.Cineplex;
import model.Movie;
import model.MovieSession;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class BookingSystem {

    private ArrayList<Movie> movies = new ArrayList<>();
    private ArrayList<Cineplex> cineplexes = new ArrayList<>();

    public BookingSystem() {
        this.loadDataFromPersistentStoarge();
    }

    private void loadDataFromPersistentStoarge() {
        // TODO: load mock data from disk rather than hard coded values

        // movies
        this.movies.add(new Movie("Theory of Everything"));
        this.movies.add(new Movie("Ant Life"));
        this.movies.add(new Movie("Mission Impossible"));
        this.movies.add(new Movie("Pope Francis: A Man of His Word"));
        this.movies.add(new Movie("Lu Over the Wall"));
        this.movies.add(new Movie("Avengers: Infinity War"));
        this.movies.add(new Movie("On Chesil Beach"));
        this.movies.add(new Movie("Overboard"));
        this.movies.add(new Movie("I Feel Pretty"));
        this.movies.add(new Movie("Rampage"));
        this.movies.add(new Movie("Overboard"));
        this.movies.add(new Movie("A Quite Place"));
        this.movies.add(new Movie("First Reformed"));
        this.movies.add(new Movie("Life of the Party"));
        this.movies.add(new Movie("Breaking In (2018)"));

        // cineplexes
        Cineplex stKilda = new Cineplex("St Kilda");
        Cineplex fitzroy = new Cineplex("Fitzroy");
        Cineplex melbourneCDB = new Cineplex("Melbourne CDB");
        Cineplex sunshine = new Cineplex("Sunshine");
        Cineplex lilydale = new Cineplex("Lilydale");

        // movie sessions
        fitzroy.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(1)));
        fitzroy.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(5)));
        fitzroy.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(4)));
        fitzroy.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(5)));
        fitzroy.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(6)));

        stKilda.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(0)));
        stKilda.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(1)));
        stKilda.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(2)));
        stKilda.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(3)));
        stKilda.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(4)));

        stKilda.addMovieSession(new MovieSession(1, LocalDateTime.now().plusDays(1), this.movies.get(0)));
        stKilda.addMovieSession(new MovieSession(2, LocalDateTime.now().plusDays(1).plusHours(2), this.movies.get(4)));
        stKilda.addMovieSession(new MovieSession(3, LocalDateTime.now().plusDays(1).plusHours(4), this.movies.get(6)));

        melbourneCDB.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(0)));
        melbourneCDB.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(1)));
        melbourneCDB.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(2)));
        melbourneCDB.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(3)));
        melbourneCDB.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(4)));

        melbourneCDB.addMovieSession(new MovieSession(1, LocalDateTime.now().plusDays(1), this.movies.get(0)));
        melbourneCDB.addMovieSession(new MovieSession(2, LocalDateTime.now().plusDays(1).plusHours(2), this.movies.get(5)));
        melbourneCDB.addMovieSession(new MovieSession(3, LocalDateTime.now().plusDays(1).plusHours(4), this.movies.get(9)));

        sunshine.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(11)));
        sunshine.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(14)));
        sunshine.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(13)));
        sunshine.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(1)));
        sunshine.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(12)));

        lilydale.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(12)));
        lilydale.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(11)));
        lilydale.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(14)));
        lilydale.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(4)));
        lilydale.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(8)));

        // add cineplexes
        this.cineplexes.add(stKilda);
        this.cineplexes.add(fitzroy);
        this.cineplexes.add(melbourneCDB);
        this.cineplexes.add(sunshine);
        this.cineplexes.add(lilydale);
    }

    public String getTodaysMovieSessions() {
        StringBuilder buf = new StringBuilder();

        for (Cineplex cineplex : this.cineplexes) {
            buf.append(cineplex.getTodaysMovieSessions());
        }
        return buf.toString();
    }

    public String getThisWeeksMovieSessions() {
        StringBuilder buf = new StringBuilder();

        for (Cineplex cineplex : this.cineplexes) {
            buf.append(cineplex.getThisWeeksMovieSessions());
        }
        return buf.toString();
    }
}
