package model;

public class Movie {

    private static int idCount = 0;

    private int id;
    private String title;

    public Movie(String title) {
        this.title = title;

        this.id = ++idCount;
    }

    public int getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

}
