package model;

import system.DateHelper;

import java.time.LocalDateTime;

public class MovieSession {

    private static int idCount = 0;

    private int id;
    private int showNumber;
    private LocalDateTime showingTime;
    private Movie movie;

    public MovieSession(int showNumber, LocalDateTime showingTime, Movie movie) {
        this.showNumber = showNumber;
        this.showingTime = showingTime;
        this.movie = movie;

        this.id = ++idCount;
    }

    public LocalDateTime getDate() {
        return this.showingTime;
    }

    public String getInfo() {
        return String.format("ID: %d, Show Number: %d, Time: %s, Movie: %s", this.id, this.showNumber,
                DateHelper.getFormattedDate(this.showingTime), this.movie.getTitle());
    }

}
