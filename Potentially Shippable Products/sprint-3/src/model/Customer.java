package model;

import java.io.Serializable;

public class Customer implements Serializable {

    private static int idCount = 0;

    private int id;
    private String email;
    private String suburb;

    public Customer(String email, String suburb) {
        this.email = email;
        this.suburb = suburb;

        this.id = ++idCount;
    }

    public String getEmail() {
        return this.email;
    }

    public String getSuburb() {
        return this.suburb;
    }

    public String getInfo() {
        return String.format("Email: %s, Suburb: %s", this.email, this.suburb);
    }
}
