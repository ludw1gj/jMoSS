package model;

import system.DateHelper;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class MovieSession implements Serializable {

    private static int idCount = 0;

    private int id;
    private int showNumber;
    private LocalDateTime showingTime;
    private Movie movie;

    private ArrayList<Booking> bookings = new ArrayList<>();

    public MovieSession(int showNumber, LocalDateTime showingTime, Movie movie) {
        this.showNumber = showNumber;
        this.showingTime = showingTime;
        this.movie = movie;

        this.id = ++idCount;
    }

    public boolean addBooking(Customer customer) {
        if (this.bookings.size() == 20) {
            return false;
        }
        this.bookings.add(new Booking(this.bookings.size() + 1, customer, this));
        return true;
    }

    public boolean removeBooking(int id) {
        for (Booking booking : this.bookings) {
            if (booking.getId() == id) {
                this.bookings.remove(booking);
                return true;
            }
        }
        return false;
    }

    public ArrayList<Booking> getBookings() {
        return this.bookings;
    }

    public int getId() {
        return this.id;
    }

    public String getMovieTitle() {
        return this.movie.getTitle();
    }

    public LocalDateTime getDate() {
        return this.showingTime;
    }

    public int getSeatsAvailable() {
        return 20 - this.bookings.size();
    }

    public String getInfo() {
        return String.format("ID: %d, Show Number: %d, Time: %s, Movie: %s, Seats Available: %s", this.id, this.showNumber,
                DateHelper.getFormattedDate(this.showingTime), this.movie.getTitle(), this.getSeatsAvailable());
    }

}
