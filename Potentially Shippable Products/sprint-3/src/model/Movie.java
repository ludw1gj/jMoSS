package model;

import java.io.Serializable;

public class Movie implements Serializable {

    private static int idCount = 0;

    private int id;
    private String title;

    public Movie(String title) {
        this.title = title;

        this.id = ++idCount;
    }

    public int getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

}
