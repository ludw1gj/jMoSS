package model;

import java.io.Serializable;

public class Booking implements Serializable {

    private static int idCount = 0;

    private int id;
    private final Customer customer;
    private final MovieSession movieSession;
    private int seatNumber;

    public Booking(int seatNumber, Customer customer, MovieSession movieSession) {
        this.customer = customer;
        this.seatNumber = seatNumber;
        this.movieSession = movieSession;

        this.id = ++idCount;
    }

    public int getId() {
        return this.id;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public String getInfo() {
        return String.format("Seat Number:\n%s\nCustomer:\n%s\nMovie Session:\n%s", this.seatNumber, this.customer.getInfo(), this.movieSession.getInfo());
    }

}
