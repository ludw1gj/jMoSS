package system;

import model.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class BookingSystem {

    private ArrayList<Movie> movies = new ArrayList<>();
    private ArrayList<Cineplex> cineplexes = new ArrayList<>();

    public BookingSystem() {
        // this.saveDataToDisk();
        this.loadDataFromDisk();
    }

    public String findTodaysMovieSessions() {
        StringBuilder buf = new StringBuilder();

        for (Cineplex cineplex : this.cineplexes) {
            buf.append(cineplex.getTodaysMovieSessionsList());
        }
        return buf.toString();
    }

    public String findThisWeeksMovieSessions() {
        StringBuilder buf = new StringBuilder();

        for (Cineplex cineplex : this.cineplexes) {
            buf.append(cineplex.getThisWeeksMovieSessionsList());
        }
        return buf.toString();
    }

    public String findTodaysMovieSessionsForCineplex(int cineplexId) {
        for (Cineplex cineplex : this.cineplexes) {
            if (cineplex.getId() == cineplexId) {
                return cineplex.getTodaysMovieSessionsList();
            }
        }
        return "No Cineplex by that ID.";
    }

    public String findMovieSessionsForCineplex(int cineplexId) {
        for (Cineplex cineplex : this.cineplexes) {
            if (cineplex.getId() == cineplexId) {
                return cineplex.getMovieSessionsList();
            }
        }
        return "No Cineplex by that ID.";
    }

    public String findCineplexesInfo() {
        StringBuilder buf = new StringBuilder();
        buf.append("Cineplex Theatres:\n");
        for (Cineplex cineplex : this.cineplexes) {
            buf.append(String.format("%s\n", cineplex.getInfo()));
        }
        return buf.toString();
    }

    public String findCineplexInfo(int cineplexId) {
        for (Cineplex cineplex : this.cineplexes) {
            if (cineplex.getId() == cineplexId) {
                return cineplex.getInfo();
            }
        }
        return "No Cineplex by that ID.";
    }

    public MovieSession findMovieSession(int movieSessionId) {
        for (Cineplex cineplex : this.cineplexes) {
            for (MovieSession movieSession : cineplex.getMovieSessions()) {
                if (movieSession.getId() == movieSessionId) {
                    return movieSession;
                }
            }
        }
        return null;
    }

    public boolean makeBooking(int cineplexId, int movieSessionId, Customer customer) {
        for (Cineplex cineplex : this.cineplexes) {
            if (cineplex.getId() == cineplexId) {
                for (MovieSession movieSession : cineplex.getMovieSessions()) {
                    if (movieSession.getId() == movieSessionId) {
                        return movieSession.addBooking(customer);
                    }
                }
            }
        }
        return false;
    }

    public ArrayList<Booking> getBookings(Customer customer) {
        ArrayList<Booking> bookings = new ArrayList<>();

        for (Cineplex cineplex : this.cineplexes) {
            for (MovieSession movieSession : cineplex.getMovieSessions()) {
                for (Booking booking : movieSession.getBookings()) {
                    if (booking.getCustomer().getEmail().equals(customer.getEmail()) &&
                            booking.getCustomer().getSuburb().equals(customer.getSuburb())) {
                        bookings.add(booking);
                    }
                }
            }
        }
        return bookings;
    }

    public boolean deleteBooking(int id) {
        for (Cineplex cineplex : this.cineplexes) {
            for (MovieSession movieSession : cineplex.getMovieSessions()) {
                for (Booking booking : movieSession.getBookings()) {
                    if (booking.getId() == id)
                        return movieSession.removeBooking(id);
                }
            }
        }
        return false;
    }

    public String findMovieSessionsForMovie(String title) {
        StringBuilder buf = new StringBuilder();

        for (Cineplex cineplex : this.cineplexes) {
            for (MovieSession movieSession : cineplex.getMovieSessions()) {
                if (movieSession.getMovieTitle().equals(title)) {
                    buf.append(String.format("%s Cineplex - %s\n", cineplex.getLocation(), movieSession.getInfo()));
                }
            }
        }
        return buf.toString();
    }

    public String findTicketsPurchasedByCustomer(String customerEmail) {
        StringBuilder buf = new StringBuilder();

        for (Cineplex cineplex : this.cineplexes) {
            for (MovieSession movieSession : cineplex.getMovieSessions()) {
                for (Booking booking : movieSession.getBookings()) {
                    if (booking.getCustomer().getEmail().equals(customerEmail)) {
                        buf.append(String.format("%s Cineplex - %s\n", cineplex.getLocation(), movieSession.getInfo()));
                    }
                }
            }
        }

        if (buf.length() == 0) {
            return "No Tickets found.";
        }
        return buf.toString();
    }

    private void saveDataToDisk() {
        // THIS METHOD IS USED IN DEVELOPMENT ONLY

        // movies
        this.movies.add(new Movie("Theory of Everything"));
        this.movies.add(new Movie("Ant Life"));
        this.movies.add(new Movie("Mission Impossible"));
        this.movies.add(new Movie("Pope Francis: A Man of His Word"));
        this.movies.add(new Movie("Lu Over the Wall"));
        this.movies.add(new Movie("Avengers: Infinity War"));
        this.movies.add(new Movie("On Chesil Beach"));
        this.movies.add(new Movie("Overboard"));
        this.movies.add(new Movie("I Feel Pretty"));
        this.movies.add(new Movie("Rampage"));
        this.movies.add(new Movie("Overboard"));
        this.movies.add(new Movie("A Quite Place"));
        this.movies.add(new Movie("First Reformed"));
        this.movies.add(new Movie("Life of the Party"));
        this.movies.add(new Movie("Breaking In (2018)"));

        // cineplexes
        Cineplex stKilda = new Cineplex("St Kilda");
        Cineplex fitzroy = new Cineplex("Fitzroy");
        Cineplex melbourneCDB = new Cineplex("Melbourne CDB");
        Cineplex sunshine = new Cineplex("Sunshine");
        Cineplex lilydale = new Cineplex("Lilydale");

        // movie sessions
        fitzroy.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(1)));
        fitzroy.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(5)));
        fitzroy.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(4)));
        fitzroy.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(5)));
        fitzroy.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(6)));

        stKilda.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(0)));
        stKilda.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(1)));
        stKilda.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(2)));
        stKilda.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(3)));
        stKilda.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(4)));

        stKilda.addMovieSession(new MovieSession(1, LocalDateTime.now().plusDays(1), this.movies.get(0)));
        stKilda.addMovieSession(new MovieSession(2, LocalDateTime.now().plusDays(1).plusHours(2), this.movies.get(4)));
        stKilda.addMovieSession(new MovieSession(3, LocalDateTime.now().plusDays(1).plusHours(4), this.movies.get(6)));

        melbourneCDB.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(0)));
        melbourneCDB.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(1)));
        melbourneCDB.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(2)));
        melbourneCDB.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(3)));
        melbourneCDB.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(4)));

        melbourneCDB.addMovieSession(new MovieSession(1, LocalDateTime.now().plusDays(1), this.movies.get(0)));
        melbourneCDB.addMovieSession(new MovieSession(2, LocalDateTime.now().plusDays(1).plusHours(2), this.movies.get(5)));
        melbourneCDB.addMovieSession(new MovieSession(3, LocalDateTime.now().plusDays(1).plusHours(4), this.movies.get(9)));

        sunshine.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(11)));
        sunshine.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(14)));
        sunshine.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(13)));
        sunshine.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(1)));
        sunshine.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(12)));

        lilydale.addMovieSession(new MovieSession(1, LocalDateTime.now(), this.movies.get(12)));
        lilydale.addMovieSession(new MovieSession(2, LocalDateTime.now().plusHours(2), this.movies.get(11)));
        lilydale.addMovieSession(new MovieSession(3, LocalDateTime.now().plusHours(3).plusMinutes(30), this.movies.get(14)));
        lilydale.addMovieSession(new MovieSession(4, LocalDateTime.now().plusHours(6), this.movies.get(4)));
        lilydale.addMovieSession(new MovieSession(5, LocalDateTime.now().plusHours(8), this.movies.get(8)));

        // add cineplexes
        this.cineplexes.add(stKilda);
        this.cineplexes.add(fitzroy);
        this.cineplexes.add(melbourneCDB);
        this.cineplexes.add(sunshine);
        this.cineplexes.add(lilydale);

        // save to disk
        try {
            // open file
            FileOutputStream dataFile = new FileOutputStream("data.dat");
            ObjectOutputStream save = new ObjectOutputStream(dataFile);

            // write to file
            save.writeObject(this.movies);
            save.writeObject(this.cineplexes);

            // close file.
            save.close();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private void loadDataFromDisk() {
        try {
            // open file
            FileInputStream dataFile = new FileInputStream("data.dat");
            ObjectInputStream save = new ObjectInputStream(dataFile);

            // save to objects
            this.movies = (ArrayList<Movie>) save.readObject();
            this.cineplexes = (ArrayList<Cineplex>) save.readObject();

            // close file.
            save.close();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

}
