# jMoSS
A Java-based Movie Search System

# Notes
## Default User
Username: john<br>
Password: secret 

Values available from user.txt

# Sprints

## Sprint 1
* Setting up required user facing Classes: jMoSS, LoginMenu, MainMenu

* Setting up required BookingSystem Class and the Menu Interface

* Setting up required model Classes: Cineplex, Movie, MovieSession, User

* Login Menu
* Main Menu
    * (1) Show this weeks Movie Sessions at all Cineplex theatres
    * (2) Show today’s Movie Sessions at all Cineplex theatres
* Login Functionality

## Sprint 2

* Implement Option 3. Show today’s Movie Sessions for a chosen Cineplex theatre
* Implement Option 4. Make a Booking for Movie Session
* Implement Option 5. Delete a Booking for Movie Session

## Sprint 3

* Implement Option 6. Find Movie Sessions for Movie
* Implement Option 7. Find Tickets purchased by customer
* Enable loading data from disk (a local file)
* Testing